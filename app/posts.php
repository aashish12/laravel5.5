<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class posts extends Model
{
    //

    protected  $table = 'post';

    protected $fillable = ['title', 'details', 'image', 'view_count', 'user_id', 'category_id', 'tags_id', 'status_id'];

    public $timestamps = false;
}
