<?php

namespace App\Http\Controllers\Api;

use App\posts;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BlogController extends Controller
{
    public function getBlogLists(){
        $result = posts::all();

        return response()->json(['message' => 'datas', 'data' => $result]);
    }

    public function createBlog(Request $request){

        $result = posts::insert([
            'title' => $request->input('title'),
            'details' => $request->input('details'),
            'image' => $request->input('image'),
            'view_count' => 100,
            'category_id' => $request->input('category'),
            'status_id' => 1,
            'tags_id' => $request->input('tag'),
            'user_id' => 3
        ]);

        if($result){
            return response()->json(['message' => 'data inserted', 'data' => $result]);
        }else{
            return response()->json(['message' => 'something went wrong']);

        }

    }

    public function verifyPost($id){
        $result = posts::find($id)->update(['status_id' => 2]);

        if($result){
            return response()->json(['message' => 'data updated', 'data' => $result]);
        }else{
            return response()->json(['message' => 'something went wrong']);

        }
    }


}
